# TP3 : Progressons vers le réseau d'infrastructure

## 1. Adressage

## 2. Routeur

🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :

il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8

    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s9

    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
```

il a un accès internet

```
[graig@router ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=24.10 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=25.1 ms
```

il a de la résolution de noms
```
[graig@router ~]$ dig ynov.com
ynov.com.		9691	IN	A	92.243.16.143

```

il porte le nom router.tp3*

```
[graig@router ~]$ hostname
router.tp3
```

n'oubliez pas d'activer le routage sur la machine

```
[graig@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
[graig@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
[graig@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[graig@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

# II. Services d'infra

## 1. Serveur DHCP

🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra :


checklist : 

```
[graig@dhcp ~]$ hostname
dhcp.client1.tp3

[graig@dhcp ~]$ ip a
inet 10.3.1.130/26 brd 10.3.1.191 scope global noprefixroute enp0s8

[graig@dhcp ~]$ dig ynov.com
ynov.com.		9068	IN	A	92.243.16.143

[graig@dhcp ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources: 
  services: cockpit dhcp dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

[graig@dhcp ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=61 time=24.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=61 time=24.2 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=61 time=21.10 ms
^C

[graig@dhcp ~]$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=61 time=23.6 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=61 time=24.10 ms
^C
```
- porter le nom dhcp.client1.tp3
- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable
```
[graig@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new

# specify domain name

option domain-name     "dhcp.client1.tp3";
# specify DNS server's hostname or IP address

option domain-name-servers     1.1.1.1;
# default lease time

default-lease-time 600;
# max lease time

max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask

subnet 10.3.1.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.131 10.3.1.189;
    # specify broadcast address
    option broadcast-address 10.3.1.191;
    # specify gateway
    option routers 10.3.1.190;
}
```

📁 Fichier [dhcpd.conf](https://gitlab.com/kolodziejczykTp/tp-reseau/tp1/1/-/blob/main/dhcpd.conf)

## VM marcel.client1.tp3

- checklist :

```
[graig@marcel ~]$ hostname
marcel.client1.tp3
```

```
[graig@marcel ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group
    inet 10.3.1.131/26 brd 10.3.1.191 scope global dynamic noprefixroute enp0s8
```

```
[graig@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
NAME=enp0s8
DEVICE=enp0s8
BOOTPROTO=dhcp
ONBOOT=yes
```

```
[graig@marcel ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

```
[graig@marcel ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=61 time=25.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=61 time=24.6 ms
^C
```

```
[graig@marcel ~]$ dig ynov.com
ynov.com.		9199	IN	A	92.243.16.143
```

```
[graig@marcel ~]$ traceroute 10.3.1.190
traceroute to 10.3.1.190 (10.3.1.190), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  1.319 ms !X  1.270 ms !X  1.247 ms !X
```

```
[graig@marcel ~]$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=61 time=22.4 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=61 time=26.3 ms
^C
```

# 2.Serveur

## B. SETUP copain

checklist :

```
[graig@dns1 ~]$ hostname
dns1.server1.tp3
```

```
user@MacBook-Pro-de-User ~ % ssh graig@10.3.1.2
The authenticity of host '10.3.1.2 (10.3.1.2)' can't be established.
ECDSA key fingerprint is SHA256:E4GP6UOPksrJGAduj584LYmLO61b00pzIi1Zt2JbDIw.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.3.1.2' (ECDSA) to the list of known hosts.
graig@10.3.1.2's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Oct 13 08:59:55 2021
```

```
[graig@dns1 ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=24.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=25.1 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=28.2 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=63 time=23.10 ms
^C
```

```
[graig@dns1 ~]$ ping 10.3.1.190
PING 10.3.1.190 (10.3.1.190) 56(84) bytes of data.
64 bytes from 10.3.1.190: icmp_seq=1 ttl=63 time=1.28 ms
64 bytes from 10.3.1.190: icmp_seq=2 ttl=63 time=0.614 ms
64 bytes from 10.3.1.190: icmp_seq=3 ttl=63 time=0.670 ms
^C
```

```
[graig@dns1 ~]$ dig google.com
google.com.		120	IN	A	142.250.201.174
```
il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme google.com

```
[graig@dns1 ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search home server1.tp3
nameserver 1.1.1.1
```

conf classique avec le fichier /etc/resolv.conf ou les fichiers de conf d'interface


comme pour le DHCP, on part sur "rocky linux dns server" on Google pour les détails de l'install du serveur DNS

le paquet que vous allez installer devrait s'appeler bind : c'est le nom du serveur DNS le plus utilisé au monde



il y aura plusieurs fichiers de conf :

un fichier de conf principal named.conf

des fichiers de zone "forward"

permet d'indiquer une correspondance nom -> IP
un fichier par zone forward


vous ne mettrez pas en place de zones reverse, uniquement les forward
on ne met PAS les clients dans les fichiers de zone car leurs adresses IP peuvent changer (ils les récupèrent à l'aide du DHCP)

donc votre DNS gérera deux zones : server1.tp3 et server2.tp3

les réseaux où les IPs sont définies de façon statique !

```
[graig@dns1 ~]$ sudo dnf install -y bind bind-chroot
```

```
[graig@dns1 ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
    listen-on port 53 { 127.0.0.1;10.3.1.2;};
    listen-on-v6 port 53 { ::1; };
    directory     "/var/named";
    dump-file     "/var/named/data/cache_dump.db";
    statistics-file "/var/named/data/named_stats.txt";
    memstatistics-file "/var/named/data/named_mem_stats.txt";
    secroots-file    "/var/named/data/named.secroots";
    recursing-file    "/var/named/data/named.recursing";
    allow-query     { localhost;any;};

    /*
     - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
     - If you are building a RECURSIVE (caching) DNS server, you need to enable
       recursion.
     - If your recursive DNS server has a public IP address, you MUST enable access
       control to limit queries to your legitimate users. Failing to do so will
       cause your server to become part of large scale DNS amplification
       attacks. Implementing BCP38 within your network would greatly
       reduce such attack surface
    */
    recursion yes;

    dnssec-enable yes;
    dnssec-validation yes;

    managed-keys-directory "/var/named/dynamic";

    pid-file "/run/named/named.pid";
    session-keyfile "/run/named/session.key";

    /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
    include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" IN {
    type master;
    file "/etc/server1.tp3.forward";
    allow-update { none;};
};

zone "server2.tp3" IN {
    type master;
    file "/etc/server2.tp3.forward";
    allow-update { none; };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

```
[graig@dns1 ~]$ sudo cat forward.server1.tp3
$TTL 86400
@   IN  SOA dns1.server1.tp3. admin.server1.tp3. (
        2021062301   ; serial
        3600         ; refresh
        1800         ; retry
        604800       ; expire
        86400 )      ; minimum TTL
;
; define nameservers
    IN  NS  dns1.server1.tp3.
;
; DNS Server IP addresses and hostnames
dns1 IN  A   10.3.1.2
;
;client records
router IN  A   10.3.1.126
```

```
[graig@dns1 ~]$ sudo cat forward.server2.tp3
$TTL 86400
@   IN  SOA dns1.server2.tp3. admin.server2.tp3. (
        2021062301   ; serial
        3600         ; refresh
        1800         ; retry
        604800       ; expire
        86400 )      ; minimum TTL
;
; define nameservers
    IN  NS  dns1.server1.tp3.
;
; DNS Server IP addresses and hostnames
dns1 IN  A   10.3.1.2
;
;client records
router IN  A   10.3.1.206
```

🌞 Tester le DNS depuis marcel.client1.tp3

définissez manuellement l'utilisation de votre serveur DNS
essayez une résolution de nom avec dig

une résolution de nom classique

```
[graig@marcel ~]$ dig ynov.com
ynov.com.		10795	IN	A	92.243.16.143
```

dig <NOM> pour obtenir l'IP associée à un nom
on teste la zone forward

```
[graig@marcel ~]$ dig google.com @8.8.8.8
google.com.		254	IN	A	216.58.213.78
```

prouvez que c'est bien votre serveur DNS qui répond pour chaque dig

```
[graig@dns1 ~]$ dig router.tp3 | grep ";; SERVER"
;; SERVER: 192.168.1.1#53(192.168.1.1)
```

# 3.Get deeper

```
[graig@marcel ~]$ dig google.com
google.com.		25	IN	A	142.250.75.238

[graig@marcel ~]$ dig google.com @8.8.8.8
google.com.		294	IN	A	142.250.178.142
```


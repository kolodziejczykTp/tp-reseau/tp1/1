# TP3 : Progressons vers le réseau d'infrastructure

## 1. Adressage

## 2. Routeur

🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :

il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8

    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s9

    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
```

il a un accès internet

```
[graig@router ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=24.10 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=25.1 ms
```

il a de la résolution de noms
```
[graig@router ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31338
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		9691	IN	A	92.243.16.143

;; Query time: 29 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: mar. oct. 05 12:32:49 CEST 2021
;; MSG SIZE  rcvd: 53
```

il porte le nom router.tp3*

```
[graig@router ~]$ hostname
router.tp3
```

n'oubliez pas d'activer le routage sur la machine

```
[graig@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
[graig@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
[graig@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[graig@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

# II. Services d'infra

## 1. Serveur DHCP

🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra :

- porter le nom dhcp.client1.tp3
- donner une IP aux machines clients qui le demande
- leur donner l'adresse de leur passerelle
- leur donner l'adresse d'un DNS utilisable
```
[graig@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new

# specify domain name

option domain-name     "dhcp.client1.tp3";
# specify DNS server's hostname or IP address

option domain-name-servers     1.1.1.1;
# default lease time

default-lease-time 600;
# max lease time

max-lease-time 7200;
# this DHCP server to be declared valid

authoritative;
# specify network address and subnetmask

subnet 10.3.1.128 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.131 10.3.1.189;
    # specify broadcast address
    option broadcast-address 10.3.1.191;
    # specify gateway
    option routers 10.3.1.190;
}
```

📁 Fichier [dhcpd.conf](https://gitlab.com/kolodziejczykTp/tp-reseau/tp1/1/-/blob/main/dhcpd.conf)
